"""
Defining data model of the books manager application
"""
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from database import Base, db_session

class Author(Base):
    """ Stores information about book authors """
    __tablename__ = 'authors'
    id = Column(Integer, primary_key=True)
    name = Column(String(80), unique=True)
    books = relationship('Book', back_populates="author")

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Author %r>' % self.name

    def save(self):
        db_session.add(self)
        db_session.commit()


class Book(Base):
    """ Stores information about books """
    __tablename__ = 'books'
    id = Column(Integer, primary_key=True)
    title = Column(String(80), unique=True)
    author = relationship('Author', back_populates="books")
    author_id = Column(Integer, ForeignKey('authors.id'))

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return '<Book %r>' % self.title

    def save(self):
        db_session.add(self)
        db_session.commit()
