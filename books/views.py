"""
Views functions, responsible for rendering items
"""
from flask import render_template, request
from flask import redirect, url_for, abort, session, flash

from books import app
from books.models import Author, Book
from books.forms import EditBook, EditAuthor
from database import db_session

@app.route('/login/', methods=['GET', 'POST'])
def login():
    """ Simple login implementation, which uses settings to login a user """
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('authors'))
    return render_template('login.html', error=error)


@app.route('/logout/')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('authors'))


@app.route('/')
def authors():
    """ Rendering a list of authors """
    return render_template('authors.html', authors=Author.query.all())


@app.route('/books/')
def books():
    """ Rendering a list of books """
    return render_template('books.html', books=Book.query.all())


@app.route('/search/')
def search():
    """ Function which implements simple queries for the database
    to be replaced with better solution.
    """
    query = request.args.get('query', None)
    if not query:
        books = Book.query.all()
    else:
        books = []
        # Getting all books written by all authors matching queue
        authors = Author.query.filter(Author.name.contains(query))
        for author in authors:
            for book in author.books:
                books.append(book)
        # Appending all book where name match query
        for book in Book.query.filter(Book.title.contains(query)).all():
            books.append(book)
    return render_template('books.html', books=books)


@app.route('/books/edit/<int:id>/', methods=['GET', 'POST'])
def edit_book(id):
    """ Function resonsible for adding and editing books """
    logged_in = session.get('logged_in', None)
    if not logged_in:
        return redirect(url_for('login'))
    # Pre fill form with data from the model in case,
    # entity is already in the database
    book = Book.query.get(id)
    form = EditBook(request.form, obj=book)
    form.authors.choices = [(author.id, author.name)
                            for author in Author.query.all()]
    if request.method == 'GET' and book:  # Fill Multiple select with authors
        form.authors.data = book.author.id
    if request.method == 'POST' and form.validate():
        book = Book.query.get(id)
        if book:
            book.title = form.title.data
        else:
            book = Book(title=form.title.data)
            book.author = Author.query.get(form.authors.data)
        book.save()
        return redirect(url_for('books'))
    return render_template('edit_book.html', form=form, item_id=id)


@app.route('/authors/edit/<int:id>/', methods=['GET', 'POST'])
def edit_author(id):
    """ Function resonsible for editing and adding authors """
    logged_in = session.get('logged_in', None)
    if not logged_in:
        return redirect(url_for('login'))
    # Pre fill form with data from the model in case,
    # entity is already in the database
    author = Author.query.get(id)
    form = EditAuthor(request.form, obj=author)
    form.books.choices = [(book.id, book.title)
                          for book in Book.query.all()]
    if request.method == 'GET' and author:  # Fill Multiple select with books
        form.books.data = [book.id for book in author.books]
    if request.method == 'POST' and form.validate():
        author = Author.query.get(id)
        if author:
            author.name = form.name.data
        else:
            author = Author(name=form.name.data)
        books = [Book.query.get(book_id) for book_id in form.books.data]
        author.books = books
        author.save()
        return redirect(url_for('authors'))
    return render_template('edit_author.html', form=form, item_id=id)


@app.route('/authors/edit/<int:id>/delete/', methods=['GET'])
def delete_author(id):
    author = Author.query.get(id)
    _delete_item(author)
    return redirect(url_for('authors'))


@app.route('/books/edit/<int:id>/delete/', methods=['GET'])
def delete_book(id):
    book = Book.query.get(id)
    _delete_item(book)
    return redirect(url_for('authors'))


def _delete_item(item):
    if item is None:
        abort(404)
    db_session.delete(item)
    db_session.commit()


def _check_logged_in(session):
    logged_in = session.get('logged_in', None)
    if not logged_in:
        return redirect(url_for('login'))
