from wtforms import Form, TextField, SelectField, SelectMultipleField
from wtforms import validators

class EditBook(Form):
    title = TextField('title', validators=[validators.InputRequired()])
    authors = SelectField('authors', coerce=int)


class EditAuthor(Form):
    name = TextField('name', validators=[validators.InputRequired()])
    books = SelectMultipleField('books', coerce=int)
