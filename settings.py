""" Application settings """
import os

LOCAL_POSTGRES = "postgresql://testpin:pinpassword@localhost/blog"
SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL", LOCAL_POSTGRES)
DEBUG = True
SECRET_KEY = 'N1fUFLnbRTgIoCIidbIop2FjM'
USERNAME = 'demo'
PASSWORD = 'test'
