""" Main entry point of the application """
from books import app
from database import db_session


@app.teardown_appcontext
def shutdown_session(exception=None):
    """
    This function will automatically remove db session at the
    end of request, or app shut down
    """
    db_session.remove()


app.config.from_object('settings')
